# upload changes with date now
up:
	git add . ;\
	git commit -m "Update: `date +'%Y-%m-%d %H:%M:%S'`" ;\
	git push

# upload submodule changes with date now
sup:
	git submodule sync --recursive ;\
	cd themes/sansoul ;\
	git fetch ;\
	git checkout origin/master ;\
	git branch master -f ;\
	git checkout master ;\
	git add . ;\
	git commit -m "Update submodule: `date +'%Y-%m-%d %H:%M:%S'`" ;\
	git push ;\
	cd ../..

# pull of repository and update the submodules
down:
	git pull ;\
	git submodule update --recursive --remote

# hugo server with theme config
server:
	hugo server --config themes/sansoul/config.default.yml,config.yml

fonts:
	PROYECT=$${PWD##*/} ;\
	cd ../_tools ;\
	sed -i ".bak" "s/^const domain = '.*'/const domain = '$${PROYECT}'/g" gulpfile.js ;\
	rm *.bak ;\
	gulp fonts
